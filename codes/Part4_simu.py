# import
import csv
import random
import pandas as pd

# Class


class Food:
    courseName = ""
    foodName = ""
    price = 0

    def __init__(self, foodName, price, courseName) -> None:
        self.foodName = foodName
        self.price = price
        self.courseName = courseName

    def __str__(self) -> str:
        return self.foodName

    def __repr__(self):
        return str(self)


class ClientType:
    typeName = ""
    lunchChance = 0
    startersPctgs = {}
    mainsPctgs = {}
    dessertsPctgs = {}

    def __init__(self, typeName, lunchChance):
        self.typeName = typeName
        self.lunchChance = lunchChance

    def __str__(self) -> str:
        return self.typeName

    def __repr__(self):
        return str(self)

    def setStartersPctgs(self, foods):
        for key in foods:
            self.startersPctgs[key] = foods[key]
        return self

    def setMainsPctgs(self, foods):
        for key in foods:
            self.mainsPctgs[key] = foods[key]
        return self

    def setDessertsPctgs(self, foods):
        for key in foods:
            self.dessertsPctgs[key] = foods[key]
        return self


class ClientGenerator:
    customerPercentage = []
    customerReturnChance = []

    def __init__(self, customerPercentage, customerReturnChance):
        self.customerPercentage = customerPercentage
        self.customerReturnChance = customerReturnChance

    def getClient(self):
        global id
        clientType = random.choice(
            [key for key in customerPercentage for i in range(customerPercentage[key])])
        for key in self.customerReturnChance:
            if clientType == key:
                returningChance = self.customerReturnChance[key]
        if random.random() < returningChance:
            if customersList[clientType] != []:
                return random.choice(customersList[clientType])
            else:
                id += 1
                return Client(id, clientType)
        else:
            id += 1
            return Client(id, clientType)


class Client:
    id = 0
    clientType = None

    def __init__(self, id, clientType):
        self.id = id
        self.clientType = clientType
        customersList[self.clientType].append(self)
        # print(self)

    def __str__(self):
        return "Customer {0} with ID: {1}".format(self.clientType, self.id)

    def __repr__(self):
        return str(self)

    def getTime(self):
        if random.random() < self.clientType.lunchChance:
            return "Lunch"
        else:
            return "Dinner"

    def goToRestaurant(self):
        time = self.getTime()
        customerId = self.id
        customerType = self.clientType.typeName

        total1 = 0
        total2 = 0
        total3 = 0

        course1 = self.getStarters()
        total1 += course1.price
        drinks1 = self.getDrinks()
        total1 += drinks1

        course2 = self.getMains()
        total2 += course2.price
        drinks2 = self.getDrinks()
        total2 += drinks2

        course3 = self.getDesserts()
        total3 += course3.price
        drinks3 = self.getDrinks()
        total3 += drinks3

        data = [time, customerId, customerType, course1, course2,
                course3, drinks1, drinks2, drinks3, total1, total2, total3]

        with open('simulation_out.csv', 'a', encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerow(data)
        # f.write("{0} {1}  {2} {3}  {4}  {5}  {6}  {7}  {8}  {9}  {10}  {11} \n"
        #         .format(time, customerId, customerType, course1, course2, course3, drinks1, drinks2, drinks3, total1, total2, total3))
        # f.close()
        return course1.__str__() + course2.__str__() + course3.__str__()

    def getDrinks(self) -> float:
        # returns a random float 0 < x < 3
        return random.random() * 3

    def getStarters(self) -> Food:
        return random.choice(
            [key for key in self.clientType.startersPctgs for i in range(self.clientType.startersPctgs[key])])

    def getMains(self) -> Food:
        return random.choice(
            [key for key in self.clientType.mainsPctgs for i in range(self.clientType.mainsPctgs[key])])

    def getDesserts(self) -> Food:
        return random.choice(
            [key for key in self.clientType.dessertsPctgs for i in range(self.clientType.dessertsPctgs[key])])


# Functions

# Init
global id
id = 0

random.seed()

soup = Food("Soup", 3, "Starters")
tomatoMozarella = Food("Tomato-Mozarella", 15, "Starters")
oysters = Food("Oysters", 20, "Starters")

salad = Food("Salad", 9, "Mains")
spaghetti = Food("Spaghetti", 20, "Mains")
lobster = Food("Lobster", 40, "Mains")

iceCream = Food("Ice Cream", 15, "Desserts")
pie = Food("Pie", 10, "Desserts")

clientType1 = ClientType("Regular", 0.3).setStartersPctgs({soup: 50, tomatoMozarella: 20, oysters: 30})\
    .setMainsPctgs({salad: 20, spaghetti: 30, lobster: 50})\
    .setDessertsPctgs({iceCream: 50, pie: 50})

clientType2 = ClientType("Business", 0.4).setStartersPctgs({soup: 50, tomatoMozarella: 20, oysters: 30})\
    .setMainsPctgs({salad: 20, spaghetti: 30, lobster: 50})\
    .setDessertsPctgs({iceCream: 50, pie: 50})

clientType3 = ClientType("Healthy", 0.5).setStartersPctgs({soup: 50, tomatoMozarella: 20, oysters: 30})\
    .setMainsPctgs({salad: 20, spaghetti: 30, lobster: 50})\
    .setDessertsPctgs({iceCream: 50, pie: 50})

clientType4 = ClientType("Retirement", 0.6).setStartersPctgs({soup: 50, tomatoMozarella: 20, oysters: 30})\
    .setMainsPctgs({salad: 20, spaghetti: 30, lobster: 50})\
    .setDessertsPctgs({iceCream: 50, pie: 50})

customerPercentage = {clientType1: 25, clientType2: 25,
                      clientType3: 25, clientType4: 25}

customerReturnChance = {clientType1: 0, clientType2: 0.5,
                        clientType3: 0.7, clientType4: 0.9}
header = ['Time', 'CustomerID', 'CustomerType', 'Course1', 'Course2',
          'Course3', 'Drinks1', 'Drinks2', 'Drinks3', 'Total1', 'Total2', 'Total3']
try:
    with open('simulation_out.csv', 'x', encoding='UTF8') as f:
        writer = csv.writer(f)
        writer.writerow(header)
    # f = open("out.txt", "x")
    # f.write("#TIME  CustomerID  CustomerType    Course1 Course2 Course3 Drinks1 Drinks2 Drinks3 Total1  Total2  Total3 \n")
    # f.close()
except FileExistsError:
    pass


global customersList
customersList = {clientType1: [], clientType2: [],
                 clientType3: [], clientType4: []}

generator = ClientGenerator(customerPercentage, customerReturnChance)
currentClient = None


output = []

# Simulation

for i in range(5*365*20):
    currentClient = generator.getClient()
    currentClient.goToRestaurant()

dff = pd.read_csv('C:/Users/Tran Lan Phuong/PycharmProjects/2022_project_python/codes/simulation_out.csv')
print(dff.head(10))