import os
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import csv
from decimal import Decimal

df = pd.read_csv('C:/Users/Tran Lan Phuong/Desktop/M2 COURS/Data/part1.csv')
print(df)

# Extract data for draw distribution of the cost in each course
Lunch = df[df['TIME'] == 'LUNCH']
Dinner = df[df['TIME'] == 'DINNER']

# Cumulative the cost of 3 courses for lunch and dinner
Lunch_Cost = Lunch['FIRST_COURSE'] + Lunch['SECOND_COURSE'] + Lunch['THIRD_COURSE']
Dinner_Cost = Dinner['FIRST_COURSE'] + Dinner['SECOND_COURSE'] + Dinner['THIRD_COURSE']

# Set the interval of x-axis according to the expenses of Lunch and Dinner
L = pd.cut(Lunch_Cost, [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90],
           right=False).value_counts().sort_index()
D = pd.cut(Dinner_Cost, [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90],
           right=False).value_counts().sort_index()

# Transfer data series to the Dataframe format in order to use in seaborn function to plot
lunch = {'Lunch_cost': L.index, 'Frequency': L.values}
dinner = {'Dinner_cost': D.index, 'Frequency': D.values}
Lunch_Dataframe = pd.DataFrame(lunch)
Dinner_Dataframe = pd.DataFrame(dinner)

# Draw the distribution of the cost in each course
# Draw the cost distribution for lunch
xLunch = plt.figure(figsize=(10, 10)).add_subplot(2, 1, 1)
plt.title('Distribution in Lunch', size=20)
sns.barplot(x="Lunch_cost", y="Frequency", data=Lunch_Dataframe, palette="Set3")
xLunch.set_xlabel("Lunch_cost", size=16)
xLunch.set_ylabel("Frequency", size=16)

for x, y in zip(range(18), Lunch_Dataframe.Frequency):
    xLunch.text(x, y, '%d' % y, ha='center', va='bottom', fontsize=15, color='grey')

# Draw the cost distribution for Dinner
xDinner = plt.figure(figsize=(10, 10)).add_subplot(2, 1, 2)
plt.title('Distribution in Dinner', size=20)
sns.barplot(x="Dinner_cost", y="Frequency", data=Dinner_Dataframe, palette="Set3")
xDinner.set_xlabel("Dinner_cost", size=16)
xDinner.set_ylabel("Frequency", size=16)
for x, y in zip(range(18), Dinner_Dataframe.Frequency):
    xDinner.text(x, y, '%d' % y, ha='center', va='bottom', fontsize=15, color='grey')

# Barplots of the cost per course
# Extract cost data of each course
First_Course = df['FIRST_COURSE']
Second_Course = df['SECOND_COURSE']
Third_Course = df['THIRD_COURSE']

# Set the interval of x-axis according to the cost of per course
First = pd.cut(First_Course, [0, 5, 10, 15, 20, 25], labels=['0-5', '5-10', '10-15', '15-20', '20-25'],
               right=False).value_counts().sort_index()
Second = pd.cut(Second_Course, [0, 5, 10, 15, 20, 25, 30, 35, 40, 45],
                labels=['0-5', '5-10', '10-15', '15-20', '20-25', '25-30', '30-35', '35-40', '40-45'],
                right=False).value_counts().sort_index()
Third = pd.cut(Third_Course, [0, 5, 10, 15, 20],
               labels=['0-5', '5-10', '10-15', '15-20'], right=False).value_counts().sort_index()

# Transfer data series to the Dataframe format in order to use in seaborn function to plot
Course1 = {'Cost': First.index, 'Frequency': First.values}
Course2 = {'Cost': Second.index, 'Frequency': Second.values}
Course3 = {'Cost': Third.index, 'Frequency': Third.values}
Course1_Dataframe = pd.DataFrame(Course1)
Course2_Dataframe = pd.DataFrame(Course2)
Course3_Dataframe = pd.DataFrame(Course3)

# Draw the barplots of cost for each course
# Draw the cost distribution for first_course by using bar plot function
course1 = plt.figure(figsize=(15, 10)).add_subplot(2, 2, 1)
sns.barplot(x="Cost", y="Frequency", data=Course1_Dataframe, palette="Set3")
course1.set_xlabel("Cost", size=16)
course1.set_ylabel("Frequency", size=16)
plt.title('First_course', size=20)
for x, y in zip(range(9), Course1_Dataframe.Frequency):
    course1.text(x, y, '%d' % y, ha='center', va='bottom', fontsize=15, color='grey')

# Draw the cost distribution for second_course
course2 = plt.figure(figsize=(15, 10)).add_subplot(2, 2, 2)
sns.barplot(x="Cost", y="Frequency", data=Course2_Dataframe, palette="Set3")
course2.set_xlabel("Cost", size=16)
course2.set_ylabel("Frequency", size=16)
plt.title('Second_course', size=20)
for x, y in zip(range(11), Course2_Dataframe.Frequency):
    course2.text(x, y, '%d' % y, ha='center', va='bottom', fontsize=15, color='grey')

# Draw the cost distribution for third_course
course3 = plt.figure(figsize=(15, 10)).add_subplot(2, 2, 3)
sns.barplot(x="Cost", y="Frequency", data=Course3_Dataframe, palette="Set3")
course3.set_xlabel("Cost", size=16)
course3.set_ylabel("Frequency", size=16)
plt.title('Third_course', size=20)
for x, y in zip(range(9), Course3_Dataframe.Frequency):
    course3.text(x, y, '%d' % y, ha='center', va='bottom', fontsize=15, color='grey')


#Define 3 key value based on the price of different dishes in 3 course, as 
key1 = [3,15,20]
key2 = [9,20,25,40]
key3 = [15,10]

# Define a function to determine the cost of drinks and the name of actural food. course as, key_, k as the number of course
def return_course_drink(course, key_, k):
    if course == '0':  # Judge whether customers spend money in the course, if not, fill 0 in the cell of dish name
        return 0
    _drink = 31  # Because the cost of the drinks is never bigger than the difference in price between two dishes in one course, we choose the biggest difference as the judgment criteria
    _course = 0

    for t_key in key_:
        if Decimal(course) > 0:
            t_drink = Decimal(course) - Decimal(t_key)
            if t_drink < 0:
                continue
            if t_drink < _drink:
                _drink = t_drink
                _course = t_key
        else:
            _drink = 0

    # According to different price of dished to determine the specific dish name for each column, based on the k number to distinguish each course
    if k == '0':
        if _course == 3:
            _course = 'Soup'
        elif _course == 15:
            _course = ' Tomato-Mozarella'
        elif _course == 20:
            _course = 'Oysters'
        else:
            _course = _course
    if k == '1':
        if _course == 9:
            _course = 'Salad'
        elif _course == 20:
            _course = 'Spaghetti '
        elif _course == 25:
            _course = 'Steak'
        elif _course == 40:
            _course = 'Lobster'
        else:
            _course = _course
    if k == '2':
        if _course == 15:
            _course = 'Ice cream '
        elif _course == 10:
            _course = 'pie'
        else:
            _course = _course
    return _course, float(_drink)


# Define a function to read the part1.csv and use for-loop for appending new columns in next step.
path_file = "C:/Users/lyanl/Desktop/Python for  finance/part1.csv"


def read_file_csv(file_path):
    t_list = []
    with open(path_file, "r") as f:
        f_csv = csv.reader(f)
        for row in f_csv:
            t_list.append(row)
    return t_list


# Def a function to creat a new csv
def write_file_csv(file_path, data, header=None):
    with open(file_path, 'w', ) as f:
        f_csv = csv.writer(f)
        f_csv.writerow(header)
        f_csv.writerows(data)


# Read the file and reconstruct the position of the column, and add the new 6 columns
data_list = read_file_csv(path_file)
new_header = ['CLIENT_ID', 'TIME', 'FIRST_COURSE', "FIRST_DISH", "FIRST_Drink_COST", 'SECOND_COURSE', "SECOND_DISH",
              "SECOND_Drink_COST", 'THIRD_COURSE', 'THIRD_DISH', "THIRD_Drink_COST"]
new_data_list = []

# Creat new data tabel named part1+6col, including the 3 columns of drink cost and 3 columns of actural food name to display the result and also conveninent for dealing with the later steps.
for data in data_list[1:]:
    FIRST_COURSE = data[2]
    SECOND_COURSE = data[3]
    THIRD_COURSE = data[4]
    FIRST_Drink_COST = return_course_drink(FIRST_COURSE, key1, '0')[1]
    FIRST_COST = return_course_drink(FIRST_COURSE, key1, '0')[0]
    SECOND_Drink_COST = return_course_drink(SECOND_COURSE, key2, '1')[1]
    SECOND_COST = return_course_drink(SECOND_COURSE, key2, '1')[0]
    THIRD_Drink_COST = return_course_drink(THIRD_COURSE, key3, '2')[1]
    THIRD_COST = return_course_drink(THIRD_COURSE, key3, '2')[0]
    new_data_list.append(
        data[:2] + [FIRST_COURSE, FIRST_COST, FIRST_Drink_COST, SECOND_COURSE, SECOND_COST, SECOND_Drink_COST,
                    THIRD_COURSE, THIRD_COST, THIRD_Drink_COST])
write_file_csv("C:/Users/lyanl/Desktop/Python for  finance/part1+6col.csv", new_data_list, new_header)

#Read the new file to show the table with 6 new columns
df = pd.read_csv('C:/Users/lyanl/Desktop/Python for  finance/part1+6col.csv')
df
