import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tabulate import tabulate

df1 = pd.read_csv('C:/Users/Tran Lan Phuong/PycharmProjects/2022_project_python/datas/label.csv')

# _______________________
# Compare the labels to the actual client type and a new column of "Comparision"
part3 = pd.read_csv('C:/Users/Tran Lan Phuong/PycharmProjects/2022_project_python/datas/part3.csv')
df1['Comparision'] = np.where(part3['CLIENT_TYPE'] == df1['CLIENT_TYPE'], True, False)

# Compare the labels with actual type by counting the numbers of each type
df1['CLIENT_TYPE'].value_counts()
part3['CLIENT_TYPE'].value_counts()

# Make a new table to add in the 'CLUSTER_LABEL', 'CLIENT_TYPE' columns, for the convenience of calling data
df2 = pd.read_csv('C:/Users/Tran Lan Phuong/PycharmProjects/2022_project_python/datas/part16col.csv')
df = pd.concat([df2, df1['CLUSTER_LABEL'], df1['CLIENT_TYPE']], axis=1)


# __________________________
# Determine the distribution of packages
# Define a function to draw the distribution of packages
def draw_Distribution_clients(data):
    # Calculate how many client of each group
    num1 = str(data['CLUSTER_LABEL'].tolist()).count('0')
    num2 = str(data['CLUSTER_LABEL'].tolist()).count('1')
    num3 = str(data['CLUSTER_LABEL'].tolist()).count('2')
    num4 = str(data['CLUSTER_LABEL'].tolist()).count('3')

    # Draw the bar plot of packages distribution
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    customer_type = ('Business', 'Onetime', 'Retirement', 'Healthy')
    quantity = (num1, num2, num3, num4)
    plt.bar(customer_type, quantity, color=["wheat", "lightskyblue", "palegreen", "plum"])
    plt.xlabel('Type of Clients', size=15)
    plt.ylabel('Number of Clients', size=15)
    plt.title('Distribution of Clients', size=20)

    # Add bar label for each bar
    for i in range(4):
        plt.text(i, quantity[i] + 0.1, "%s" % quantity[i], ha='center', va='bottom', fontsize=15)
    plt.show()


# Call the draw_Distribution_clients function
draw_Distribution_clients(df)

# __________________________________
# The likelihood for each of these packages to get a certain course
# __________ method:

# calculate total number of each group of packages
b = np.sum(df['CLIENT_TYPE'] == "Business")
o = np.sum(df['CLIENT_TYPE'] == "Onetime")
r = np.sum(df['CLIENT_TYPE'] == "Retirement")
h = np.sum(df['CLIENT_TYPE'] == "Healthy")

# amount of each group of packages following each dish
b1 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['FIRST_DISH'] != '0')])
b2 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['SECOND_DISH'] != '0')])
b3 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['THIRD_DISH'] != '0')])

o1 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['FIRST_DISH'] != '0')])
o2 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['SECOND_DISH'] != '0')])
o3 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['THIRD_DISH'] != '0')])

r1 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['FIRST_DISH'] != '0')])
r2 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['SECOND_DISH'] != '0')])
r3 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['THIRD_DISH'] != '0')])

h1 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['FIRST_DISH'] != '0')])
h2 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['SECOND_DISH'] != '0')])
h3 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['THIRD_DISH'] != '0')])

# create datas from calculating the percentage of likelihood of using dish of each group of packages
data = [["first course", b1 / b, o1 / o, r1 / r, h1 / h],
        ["second course", b2 / b, o2 / o, r2 / r, h2 / h],
        ["third course", b3 / b, o3 / o, r3 / r, h3 / h]]

col_names = ["", "business", "onetime", "retirement", "healthy"]
print(tabulate(data, headers=col_names, tablefmt="fancy_grid"))

# _________________________
# Determine the probability of a certain type of customer ordering a certain dish
bb1 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['FIRST_DISH'] == 'Soup')])
bb2 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['FIRST_DISH'] == ' Tomato-Mozarella')])
bb3 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['FIRST_DISH'] == 'Oysters')])
bb4 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['SECOND_DISH'] == 'Salad')])
bb5 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['SECOND_DISH'] == 'Spaghetti ')])
bb6 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['SECOND_DISH'] == 'Steak')])
bb7 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['SECOND_DISH'] == 'Lobster')])
bb8 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['THIRD_DISH'] == 'Ice cream ')])
bb9 = len(df.loc[(df['CLIENT_TYPE'] == "Business") & (df['THIRD_DISH'] == 'pie')])

oo1 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['FIRST_DISH'] == 'Soup')])
oo2 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['FIRST_DISH'] == ' Tomato-Mozarella')])
oo3 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['FIRST_DISH'] == 'Oysters')])
oo4 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['SECOND_DISH'] == 'Salad')])
oo5 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['SECOND_DISH'] == 'Spaghetti ')])
oo6 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['SECOND_DISH'] == 'Steak')])
oo7 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['SECOND_DISH'] == 'Lobster')])
oo8 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['THIRD_DISH'] == 'Ice cream ')])
oo9 = len(df.loc[(df['CLIENT_TYPE'] == "Onetime") & (df['THIRD_DISH'] == 'pie')])

rr1 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['FIRST_DISH'] == 'Soup')])
rr2 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['FIRST_DISH'] == ' Tomato-Mozarella')])
rr3 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['FIRST_DISH'] == 'Oysters')])
rr4 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['SECOND_DISH'] == 'Salad')])
rr5 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['SECOND_DISH'] == 'Spaghetti ')])
rr6 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['SECOND_DISH'] == 'Steak')])
rr7 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['SECOND_DISH'] == 'Lobster')])
rr8 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['THIRD_DISH'] == 'Ice cream ')])
rr9 = len(df.loc[(df['CLIENT_TYPE'] == "Retirement") & (df['THIRD_DISH'] == 'pie')])

hh1 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['FIRST_DISH'] == 'Soup')])
hh2 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['FIRST_DISH'] == ' Tomato-Mozarella')])
hh3 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['FIRST_DISH'] == 'Oysters')])
hh4 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['SECOND_DISH'] == 'Salad')])
hh5 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['SECOND_DISH'] == 'Spaghetti ')])
hh6 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['SECOND_DISH'] == 'Steak')])
hh7 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['SECOND_DISH'] == 'Lobster')])
hh8 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['THIRD_DISH'] == 'Ice cream ')])
hh9 = len(df.loc[(df['CLIENT_TYPE'] == "Healthy") & (df['THIRD_DISH'] == 'pie')])

data = [["soup", bb1/b, oo1/o, rr1/r, hh1/h],
        ["tomato-moz", bb2/b, oo2/o, rr2/r, hh2/h],
        ["oysters", bb3/b, oo3/o, rr3/r, hh3/h],
        ["salad", bb4/b, oo4/o, rr4/r, hh4/h],
        ["spaghetti", bb5/b, oo5/o, rr5/r, hh5/h],
        ["steak", bb6/b, oo6/o, rr6/r, hh6/h],
        ["lobster", bb7/b, oo7/o, rr7/r, hh7/h],
        ["ice cream", bb8/b, oo8/o, rr8/r, hh8/h],
        ["pie", bb9/b, oo9/o, rr9/r, hh9/h]]

col_names = ["", "business", "onetime", "retirement", "healthy"]
print(tabulate(data, headers=col_names, tablefmt="fancy_grid"))

# ____________________________________________
# Distribution of dishes per course
def draw_distribution__dishes_course(course_dish):
    tmp_course = df[course_dish]
    noRepeat = ['0']
    dish = []
    # Use for-loop to
    for i in tmp_course:
        if i not in noRepeat:
            noRepeat.append(i)
    noRepeat.remove('0')

    # To get the names of dishes--Use for loop to remove the repeat items to get the single one of each dishes
    for i in noRepeat:
        dish.append(str(tmp_course.tolist()).count(i))
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.bar(noRepeat, dish, color='wheat')
    plt.xlabel('Type of Dishes', size=15)
    plt.ylabel('Number of Orders', size=15)
    plt.title('Dishes Distribution')

    # Add the bar label for each graph
    for i in range(len(dish)):
        plt.text(i, dish[i], "%s" % dish[i], ha='center', va='bottom', size=12)
    plt.show()


# Call the function to draw the distribution graph of dishes per course
draw_distribution__dishes_course('FIRST_DISH')
draw_distribution__dishes_course('SECOND_DISH')
draw_distribution__dishes_course('THIRD_DISH')


# Determine the distribution of dishes per customer type
# Define a function to draw the distribution of dishes per customer type
def draw_distribution__dishes_type(type):
    dishs = ['Soup', 'Tomato-Mozarella', 'Oysters', 'Salad', 'Spaghetti', 'Steak', 'Lobster', 'Ice cream ', 'Pie']
    type_dishs = df[df['CLIENT_TYPE'] == type]
    dishs_count = []
    # Calculate the number of total orders for each dishes in 3 courses
    plt.figure(figsize=(10, 5))
    for j in dishs:
        count = str(type_dishs['FIRST_DISH'].tolist()).count(j)
        count += str(type_dishs['SECOND_DISH'].tolist()).count(j)
        count += str(type_dishs['THIRD_DISH'].tolist()).count(j)
        dishs_count.append(count)
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.bar(dishs, dishs_count, color='#718dbf')
    plt.subplot(1, 1, 1)
    plt.title(type, size=20)
    plt.xlabel('Type of Dishes', size=15)
    plt.ylabel('Number of Orders', size=15)
    for i in range(len(dishs_count)):
        plt.text(i, dishs_count[i], "%s" % dishs_count[i], ha='center', va='bottom')
    plt.show()


# Call the function to draw the distribution graph of dishes per customer type
draw_distribution__dishes_type('Business')
draw_distribution__dishes_type('Retirement')
draw_distribution__dishes_type('Onetime')
draw_distribution__dishes_type('Healthy')

# The distribution of the cost of the drinks per course
First_Drink = df['FIRST_Drink_COST']
Second_Drink = df['SECOND_Drink_COST']
Third_Drink = df['THIRD_Drink_COST']

a = pd.cut(First_Drink, [0, 0.5, 1, 1.5, 2, 2.5], right=False).value_counts().sort_index()
b = pd.cut(Second_Drink, [0, 1, 2, 3, 4, 5, 6], right=False).value_counts().sort_index()
c = pd.cut(Third_Drink, [0, 0.5, 1, 1.5, 2, 2.5], right=False).value_counts().sort_index()

# Transfer data to the Dataframe format in order to use in seaborn function
Drink1 = {'Cost': a.index, 'Frequency': a.values}
Drink2 = {'Cost': b.index, 'Frequency': b.values}
Drink3 = {'Cost': c.index, 'Frequency': c.values}
Drink1_Dataframe = pd.DataFrame(Drink1)
Drink2_Dataframe = pd.DataFrame(Drink2)
Drink3_Dataframe = pd.DataFrame(Drink3)

# __________________________
# Draw the Distribution graph of drink cost for each course
# For the first course
drink1 = plt.figure(figsize=(15, 10)).add_subplot(1, 1, 1)
sns.barplot(x="Cost", y="Frequency", data=Drink1_Dataframe, palette="Set3")
plt.title('Drink Cost Distribution for First_course', size=20)
drink1.set_xlabel("Cost", size=16)
drink1.set_ylabel("Frequency", size=16)

# Display frequency on every bar
for x, y in zip(range(9), Drink1_Dataframe.Frequency):
    drink1.text(x, y, '%d' % y, ha='center', va='bottom', fontsize=15, color='grey')

# Draw the Distribution graph of drink cost for second course
drink2 = plt.figure(figsize=(15, 10)).add_subplot(1, 1, 1)
sns.barplot(x="Cost", y="Frequency", data=Drink2_Dataframe, palette="Set3")
plt.title('Drink Cost Distribution for Second_course', size=20)
drink2.set_xlabel("Cost", size=16)
drink2.set_ylabel("Frequency", size=16)
for x, y in zip(range(9), Drink2_Dataframe.Frequency):
    drink2.text(x, y, '%d' % y, ha='center', va='bottom', fontsize=15, color='grey')

# Draw the Distribution graph of drink cost for third course
drink3 = plt.figure(figsize=(15, 10)).add_subplot(1, 1, 1)
sns.barplot(x="Cost", y="Frequency", data=Drink3_Dataframe, palette="Set3")
plt.title('Drink Cost Distribution for Third_course', size=20)
drink3.set_xlabel("Cost", size=16)
drink3.set_ylabel("Frequency", size=16)
for x, y in zip(range(9), Drink3_Dataframe.Frequency):
    drink3.text(x, y, '%d' % y, ha='center', va='bottom', fontsize=15, color='grey')
