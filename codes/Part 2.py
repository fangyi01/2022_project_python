import os
import pandas as pd
import numpy as np
import random
from pandas import plotting
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objs as go
import plotly.offline as py
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D
import csv

# Read document
df = pd.read_csv('C:/Users/Tran Lan Phuong/PycharmProjects/2022_project_python/datas/part1.csv')
print(df)

# Cluster clients data on the three columns of the cost
# Transfer dataframe of the three costs to array and collect into a vector in order to be used in KMeans function
cost1 = np.array(df['FIRST_COURSE'])
cost2 = np.array(df['SECOND_COURSE'])
cost3 = np.array(df['THIRD_COURSE'])
Data = np.vstack((cost1, cost2, cost3)).T

# Use Kmeans to cluster the type of clients
clients = KMeans(n_clusters=4).fit(Data)
labels = clients.labels_
df["CLUSTER_LABEL"] = labels
print(df)

# Add specific type label according to observation of clustering label.
client_type = {0: 'Business', 1: 'Retirement', 2: 'Onetime', 3: 'Healthy'}
df['CLIENT_TYPE'] = df['CLUSTER_LABEL'].map(client_type)
print(df)

# Because the labels' number obtained is different everytime when run of kmeans function,
# we saved a CSV file named label. For continuing with the following steps by read this file to avoid the label corresponding error caused by running kmeans again.
df.to_csv('C:/Users/Tran Lan Phuong/PycharmProjects/2022_project_python/datas/label.csv', sep=',', index=False, header=True)

# Figure out which is the healthy group, which is the retirement group, business and normal customers
# Set the size and projection of clustering graph
plt.figure(figsize=(12, 8))
ax = plt.subplot(projection='3d')

# Import the cost data of the three Course as coordinate axes and Set x-First, y-Second, z-Third
x = Data[:, 0]
y = Data[:, 1]
z = Data[:, 2]

# Use predict() function to predict the center
C_i = clients.predict(Data)
Cluster_Center = clients.cluster_centers_

# S et the labels and size for x, y and z axes,and the title of graph
ax.scatter3D(x, y, z, c=C_i, s=20, alpha=1)
ax.set_xlabel('Cost of Starters', size=15)
ax.set_ylabel('Cost of Mains', size=15)
ax.set_zlabel('Cost of Desserts', size=15)
ax.set_title('Clustering Clients Type', size=20)
ax.dist = 9

# Mark the center points of different client group, and the client type represented by the center point
for name, label in [('Business', 0), ('Onetime', 1), ('Retirement', 2), ('Healthy', 3)]:
    # Use for loop to add label in different center
    ax.text3D(Cluster_Center[label:label + 1, 0].mean(), Cluster_Center[label:label + 1, 1].mean(),
              Cluster_Center[label:label + 1, 2].mean() + 2, name, fontsize=20)
ax.scatter3D(Cluster_Center[:, 0], Cluster_Center[:, 1], Cluster_Center[:, 2], c='r', s=120)

# Question: Can you find out any specific characteristics per group?

# According to observing the clustering graph, we found that based on the position of clustering centers:
# For the Business clients, they prefer to eat expensive mains and desserts,and relative cheap starters.
# For the Healthy clients, they almost not eat desserts, only eat relative cheap mains and starters.
# For the Retirement clients, they will eat three course food, and the food they ate is relative cheaper than business.
# For the Onetime clients, they only eat mains, don't eat any starters and desserts.
